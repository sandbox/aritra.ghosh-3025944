<?php 
namespace Drupal\booking\Controller;

use Drupal\node\Entity\Node;
use \Drupal\Core\Access\AccessResult;
use \Drupal\Core\Access\AccessResultAllowed;
use \Drupal\Core\Access\AccessResultForbidden;

class BookingAccess {
    public function bookingFormAccess($node) {
        $node_data = Node::load($node);
        $content_type = $node_data->get('type')->getValue();
        $content_type = $content_type[0]['target_id'];
        
        if(!empty($content_type)){
            $booking_settings = \Drupal::config('node.type.' . $content_type)->get('booking');
            
            /***** Giving access to only those with bookable configuration *****/
            if(isset($booking_settings['bookable'])){
                return AccessResult::allowed();
            }else{
                return AccessResult::forbidden();
            }
            return AccessResult::forbidden();
        }
    }
}