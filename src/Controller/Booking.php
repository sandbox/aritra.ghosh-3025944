<?php 
namespace Drupal\booking\Controller;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

use Drupal\node\Entity\Node;


use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\Core\Session\AccountProxy;

class Booking extends FormBase{

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'booking';
    }
    
   /**
    * {@inheritdoc}
    */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $current_path = \Drupal::service('path.current')->getPath();
        $path_args = explode('/', $current_path);
        
        $node_data = Node::load($path_args[2]);
        $node_title = $node_data->get('title')->getValue();
        $node_title = $node_title[0]['value'];
        
        
        $form['node_name'] = array(
            '#type' => 'markup',
            '#markup' => t('<b>'.$node_title.'<b>'),
        );
        $form['bookable_entity_id'] = array(
            '#type' => 'hidden',
            '#value' => $path_args[2],
        );
        
        $form['event_code'] = array(
            '#type' => 'textfield',
            '#title' => t('Event Code:'),
            '#prefix'=> '<div class="col-sm-12"><div class="col-sm-6">',
            '#suffix'=> '</div>',
            '#required' => TRUE,
        );
        $form['no_of_unit'] = array(
            '#type' => 'textfield',
            '#title' => t('No. of units'),
            '#prefix'=> '<div class="col-sm-6">',
            '#suffix'=> '</div></div>',
            '#required' => TRUE,
        );
        $form['is_perpetual'] = array(
            '#type' => 'checkbox',
            '#title' => t('Is Perpetual'),
            '#prefix'=> '<div class="col-sm-4">',
            '#suffix'=> '</div>',
           // '#required' => TRUE,
        );
        $form ['sdate'] = array (
            '#type' => 'date',
            '#title' => 'Start Date',
            '#date_format' => 'd/m/Y',
            '#date_year_range' => '-1:+1',
            '#default_value'=>'',
            '#prefix'=> '<div class="col-sm-4">',
            '#suffix'=> '</div>',
            '#required'=>TRUE
        );
        $form ['edate'] = array (
            '#type' => 'date',
            '#title' => 'End Date',
            '#date_format' => 'd/m/Y',
            '#date_year_range' => '-1:+1',
            '#default_value'=>'',
            '#states' => array(
                'invisible' => array(
                    ':input[name="is_perpetual"]' => array('checked' => TRUE),
                ),
            ),
            '#prefix'=> '<div class="col-sm-4">',
            '#suffix'=> '</div>',
        );
        $form ['span'] = array (
            '#type' => 'textfield',
            '#title' => 'Enter Span',
            '#default_value'=>'',
            '#prefix'=> '<div class="col-sm-4">',
            '#suffix'=> '</div>',
        );
        $form ['granularity'] = array (
            '#type' => 'select',
            '#title' => 'Select Granularity',
            '#options'=>array('M'=>'Minutely','H'=>'Hourly','D'=>'Daily','W'=>'Weekly','MO'=>'Monthly','Y'=>'Yearly'),
            '#default_value'=>'',
            '#prefix'=> '<div class="col-sm-4">',
            '#suffix'=> '</div>',
        );
        $form ['status'] = array (
            '#type' => 'select',
            '#title' => 'Status',
            '#options'=>array('A'=>'Available','U'=>'Unavailable'),
            '#prefix'=> '<div class="col-sm-4">',
            '#suffix'=> '</div>',
            '#required'=>TRUE
        );
       
        $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = array(
            '#type' => 'submit',
            '#value' => $this->t('Save'),
            '#button_type' => 'primary',
        );
        
        $form['library_booking'] = array(          
            '#attached' => array(            
                'library' => array('booking/booking'),         
            ),        
            
        );
        
        return $form;
    }
    protected $database;
    
    public function __construct(Connection $database) {
        $this->database = $database;
    }
    
    public static function create(ContainerInterface $container) {
        return new static($container->get('database'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $current_user = \Drupal::currentUser();
        $current_user = $current_user->id();
        
       $total_unit = $form_state->getValue('no_of_unit');
       $span = $form_state->getValue('span');
       $granularity = $form_state->getValue('granularity');
       $sdate = strtotime($form_state->getValue('sdate'));
     
       $is_perpetual = $form_state->getValue('is_perpetual');
       if($is_perpetual == TRUE){
           $edate = strtotime($form_state->getValue('edate'));
       }else{
           $edate = strtotime($form_state->getValue('edate'));
       }
       
      /*  echo $sdate;
       echo $edate;
       exit; */
       
       if($granularity == 'D'){
          // $no_of_days = dateDiffInDays($date1, $date2);
           $no_of_days = $form_state->getValue('span');
           for($i=1;$i<=$no_of_days;$i++){
               for ($j=1;$j<=$total_unit;$j++){
                   db_insert('booking') // Table name no longer needs {}
                   ->fields(array(
                       'bookable_entity_id' => $form_state->getValue('bookable_entity_id'),
                       'unit_code' => $form_state->getValue('event_code').$j,
                       'date' => time(),
                       'stime' => $sdate,
                       'etime' => $edate,
                       'status' => $form_state->getValue('status'),
                       'uid' => $current_user,
                   ))
                   ->execute();
               }
           }
        }
    }
}